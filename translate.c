#include "translate.h"

int translate_init_module(void)
{
  int res = 0, i = 0;
  dev_t dev = 0;
  if(!major) {
    res = alloc_chrdev_region(&dev, TRANSLATE_MINOR, TRANSLATE_DEVICE_COUNT, TRANSLATE_DEVICE_NAME);
    major = MAJOR(dev);
  } 
  if(res < 0) {
    printk(KERN_WARNING "translate: cant get major");
    return res;
    
  }
  
  translate_devices = kmalloc(TRANSLATE_DEVICE_COUNT * sizeof(struct translate_dev), GFP_KERNEL);
  if(!translate_devices) {
  res = -ENOMEM;
  translate_cleanup_module();
  return res;
  }
  
    memset(translate_devices, 0, TRANSLATE_DEVICE_COUNT * sizeof(struct translate_dev));
    for(i = 0; i < TRANSLATE_DEVICE_COUNT; i++) {
    
     init_MUTEX(&translate_devices[i].sem);   
     translate_devices[i].r_count = translate_devices[i].w_count =  0;
     
     spin_lock_init(&translate_devices[i].r_lock);
     spin_lock_init(&translate_devices[i].w_lock);
     
     translate_devices[i].buf = kmalloc(bufsize * sizeof(char), GFP_KERNEL);
     translate_devices[i].buf_end = (translate_devices[i].buf+(bufsize));
     translate_devices[i].pread = translate_devices[i].pwrite = translate_devices[i].buf;
   
     translate_devices[i].fillcount = 0;      
     translate_devices[i].buf_size = bufsize;
     
     init_waitqueue_head(&(translate_devices[i].inq));
     init_waitqueue_head(&(translate_devices[i].outq));
     
     translate_setup_cdev(&translate_devices[i], i);
    }
  
  return 0;
  
}



void translate_cleanup_module(void)
{
  int i = 0;
  dev_t devno = MKDEV(major, TRANSLATE_MINOR);
   if(translate_devices) {
  
    for(i = 0; i < TRANSLATE_DEVICE_COUNT; i++) {
      cdev_del(&translate_devices[i].cdev);
      if(!translate_devices[i].buf) kfree(translate_devices[i].buf);
    }
 
   
   kfree(translate_devices);  
   }
  unregister_chrdev_region(devno, TRANSLATE_DEVICE_COUNT);
  translate_devices = 0;
}



void translate_setup_cdev(struct translate_dev *dev, int index) {
  int res = 0, dev_no = MKDEV(major, TRANSLATE_MINOR+index);
  cdev_init(&dev->cdev, &translate_fops);
  dev->cdev.owner = THIS_MODULE;
  dev->cdev.ops = &translate_fops;
  res = cdev_add(&dev->cdev, dev_no, 1);
  if(res) printk(KERN_NOTICE "Error %d adding translate dev: %d", res, index);
}

int translate_open(struct inode *inode, struct file *filp) {
    
    struct translate_dev *dev;
    dev = container_of(inode->i_cdev, struct translate_dev, cdev);
    filp->private_data = dev;
    
    if(filp->f_mode & FMODE_READ) {
      spin_lock(&dev->r_lock);
      if(!(dev->r_count < TRANSLATE_MAX_USERS)) {
     spin_unlock(&dev->r_lock);
     return -EBUSY; 
    }
       dev->r_count++;
	printk(KERN_DEBUG "open: readers:  %d", dev->r_count);
       spin_unlock(&dev->r_lock);
    }
    
    if(filp->f_mode & FMODE_WRITE) {
      spin_lock(&dev->w_lock);
    if(!(dev->w_count < TRANSLATE_MAX_USERS))  {
      spin_unlock(&dev->w_lock);
     return -EBUSY; 
    }
    dev->w_count++;
    	printk(KERN_DEBUG "open: writers: %d", dev->w_count);
    spin_unlock(&dev->w_lock);
    }
    

    if(iminor(inode) == 0) filp->f_op = &translate_fops_enc;
    if(iminor(inode) == 1) filp->f_op = &translate_fops_dec;
    

    return nonseekable_open(inode, filp);
}

int translate_release(struct inode *inode, struct file *filp){
  
  struct translate_dev *dev = filp->private_data;
  
  
    if(filp->f_mode & FMODE_READ) {
     spin_lock(&dev->r_lock);
     dev->r_count--;
     printk(KERN_DEBUG "release: readers %d", dev->r_count);
     spin_unlock(&dev->r_lock);
    }
    
    if(filp->f_mode & FMODE_WRITE) {
    spin_lock(&dev->w_lock);    
    dev->w_count--;
    printk(KERN_DEBUG "release: writers %d", dev->w_count);
    spin_unlock(&dev->w_lock);
    }    
   
   
  return 0;
}

ssize_t translate_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos) {
  
  struct translate_dev *dev = filp->private_data;
  
  
  if(down_interruptible(&dev->sem)) {
      return -ERESTARTSYS;
  }
  
  
  while((dev->pread == dev->pwrite) && (dev->fillcount==0)) {
    printk(KERN_DEBUG "read: getting ready to sleep");
    up(&dev->sem);
    
    if(	wait_event_interruptible(dev->inq, ((dev->fillcount>0)))	) {
	    return -ERESTARTSYS;
    }

    printk(KERN_DEBUG "read: returned from sleep");
    if(down_interruptible(&dev->sem)){
	    return -ERESTARTSYS;
    }
   
  }
  
  
  
  if(dev->pwrite > dev->pread) {
	
        count = min(count, (size_t)(dev->pwrite - dev->pread));
  } else {
	count = min(count, (size_t)(dev->buf_end - dev->pread));    
  }
  printk(KERN_DEBUG "read: reading from 0x%x to 0x%x (%d), end of buffer: 0x%x fillcount: %d f_pos: %d buf_start: 0x%x", (unsigned int)dev->pread, (unsigned int)dev->pread + count, count, (unsigned int)dev->buf_end, dev->fillcount, (int)*f_pos, (unsigned int)dev->buf);
  
  if(copy_to_user(buf, dev->pread, count)) {
    up(&dev->sem);
    return -EFAULT;
    
  }
  
  dev->fillcount -= count;
  dev->pread += count;
  *f_pos += count;
  if((dev->pread == dev->buf_end)) {
    dev->pread = dev->buf;
    *f_pos = 0;      
  }
  
  up(&dev->sem);  
  
  wake_up_interruptible(&dev->outq);
   
  
  return count;
}

ssize_t translate_write(struct file *filp, const char __user *buf, size_t count,loff_t *f_pos){
  struct translate_dev *dev = filp->private_data;
  
  
  if(down_interruptible(&dev->sem)) {
	  return -ERESTARTSYS;  
  }
  
   while((dev->pread == dev->pwrite) && (dev->fillcount == dev->buf_size)) {
    printk(KERN_DEBUG "write: getting ready to sleep");
    up(&dev->sem);
    
    if(wait_event_interruptible(dev->outq, (dev->fillcount < dev->buf_size))) {
	    return -ERESTARTSYS;
    }
     printk(KERN_DEBUG "write: returned from sleep");
        
    if(down_interruptible(&dev->sem)){
	    return -ERESTARTSYS;
    }

    
  }
  
  if(dev->pwrite >= dev->pread) {
       count = min(count, (size_t)(dev->buf_end - dev->pwrite));    
  } else {
  count = min(count, (size_t)(dev->pread - dev->pwrite));
  }
  printk(KERN_DEBUG "write: writing from 0x%x to 0x%x (%d), end of buffer: 0x%x fillcount: %d f_pos: %d buf start: 0x%x", (unsigned int)dev->pwrite, (unsigned int)dev->pwrite + count, count, (unsigned int)dev->buf_end, dev->fillcount, (int)*f_pos, (unsigned int)dev->buf);
  if(copy_from_user(dev->pwrite, buf, count)) {
      up(&dev->sem);
      return -EFAULT;
  }
  
  dev->fillcount += count;
  dev->pwrite += count;
  *f_pos += count;
 
  if(dev->pwrite == dev->buf_end) {
      dev->pwrite = dev->buf;
      *f_pos = 0;
    
  }
  up(&dev->sem);
  
  wake_up_interruptible(&dev->inq);
  
   
  return count;
}

void translate_caesar_encrypt(char *buf, int buf_size) {
     char alphabet[] = TRANSLATE_CAESAR_ALPHABET;
    int i = 0;
    for(i = 0; i < buf_size; i++) {
         translate_caesar_shift(1, &buf[i], alphabet, sizeof(alphabet)-1, cshift); 
    }
}

void translate_caesar_decrypt(char *buf, int buf_size) {
    char alphabet[] = TRANSLATE_CAESAR_ALPHABET;
    int i = 0;
    for(i = 0; i < buf_size; i++) {
         translate_caesar_shift(0, &buf[i], alphabet, sizeof(alphabet)-1, cshift); 
    }
}

void translate_caesar_shift(int right_shift, char *target_char, char *alphabet, int alphabet_size, int shift) {

  int i = 0, alphabet_idx = 0;
  for(i = 0; i < alphabet_size; i++) {  
    if(alphabet[i] == *target_char) {      
      if(right_shift) alphabet_idx = (i+shift) % alphabet_size;
      else alphabet_idx = (i-shift) % alphabet_size;
      if(alphabet_idx < 0) alphabet_idx += alphabet_size;
    
      *target_char = alphabet[alphabet_idx];
   
      return;
    }
    
  }
  
}



ssize_t translate_write_enc(struct file *filp, const char __user *buf, size_t count,loff_t *f_pos){
  struct translate_dev *dev = filp->private_data;
  char *temp_buf = 0;
  
  if(down_interruptible(&dev->sem)) {
	  return -ERESTARTSYS;  
  }
  
   while((dev->pread == dev->pwrite) && (dev->fillcount == dev->buf_size)) {
    printk(KERN_DEBUG "write: getting ready to sleep");
    up(&dev->sem);
    
    if(wait_event_interruptible(dev->outq, (dev->fillcount < dev->buf_size))) {
	    return -ERESTARTSYS;
    }
     printk(KERN_DEBUG "write: returned from sleep");
        
    if(down_interruptible(&dev->sem)){
	    return -ERESTARTSYS;
    }

    
  }
  
  if(dev->pwrite >= dev->pread) {
       count = min(count, (size_t)(dev->buf_end - dev->pwrite));    
  } else {
  count = min(count, (size_t)(dev->pread - dev->pwrite));
  }
  printk(KERN_DEBUG "write: writing from 0x%x to 0x%x (%d), end of buffer: 0x%x fillcount: %d f_pos: %d buf start: 0x%x", (unsigned int)dev->pwrite, (unsigned int)dev->pwrite + count, count, (unsigned int)dev->buf_end, dev->fillcount, (int)*f_pos, (unsigned int)dev->buf);
  
  temp_buf = kmalloc(count, GFP_KERNEL);
  
  
  if(copy_from_user(temp_buf, buf, count)) {
      up(&dev->sem);
      return -EFAULT;
  }
  translate_caesar_encrypt(temp_buf, count);
  memcpy(dev->pwrite, temp_buf, count);
  kfree(temp_buf);
  dev->fillcount += count;
  dev->pwrite += count;
  *f_pos += count;
 
  if(dev->pwrite == dev->buf_end) {
      dev->pwrite = dev->buf;
      *f_pos = 0;
    
  }
  up(&dev->sem);
  
  wake_up_interruptible(&dev->inq);
  
   
  return count;
}

ssize_t translate_write_dec(struct file *filp, const char __user *buf, size_t count,loff_t *f_pos){
  struct translate_dev *dev = filp->private_data;
  char *temp_buf = 0;
  
  if(down_interruptible(&dev->sem)) {
	  return -ERESTARTSYS;  
  }
  
   while((dev->pread == dev->pwrite) && (dev->fillcount == dev->buf_size)) {
    printk(KERN_DEBUG "write: getting ready to sleep");
    up(&dev->sem);
    
    if(wait_event_interruptible(dev->outq, (dev->fillcount < dev->buf_size))) {
	    return -ERESTARTSYS;
    }
     printk(KERN_DEBUG "write: returned from sleep");
        
    if(down_interruptible(&dev->sem)){
	    return -ERESTARTSYS;
    }

    
  }
  
  if(dev->pwrite >= dev->pread) {
       count = min(count, (size_t)(dev->buf_end - dev->pwrite));    
  } else {
  count = min(count, (size_t)(dev->pread - dev->pwrite));
  }
  printk(KERN_DEBUG "write: writing from 0x%x to 0x%x (%d), end of buffer: 0x%x fillcount: %d f_pos: %d buf start: 0x%x", (unsigned int)dev->pwrite, (unsigned int)dev->pwrite + count, count, (unsigned int)dev->buf_end, dev->fillcount, (int)*f_pos, (unsigned int)dev->buf);
  
  temp_buf = kmalloc(count, GFP_KERNEL);
  
  
  if(copy_from_user(temp_buf, buf, count)) {
      up(&dev->sem);
      return -EFAULT;
  }
  translate_caesar_decrypt(temp_buf, count);
  memcpy(dev->pwrite, temp_buf, count);
  kfree(temp_buf);
  dev->fillcount += count;
  dev->pwrite += count;
  *f_pos += count;
 
  if(dev->pwrite == dev->buf_end) {
      dev->pwrite = dev->buf;
      *f_pos = 0;
    
  }
  up(&dev->sem);
  
  wake_up_interruptible(&dev->inq);
  
   
  return count;
}
