 ifneq ($(KERNELRELEASE),)
	obj-m += translate.o

else

	KERNELDIR ?= /lib/modules/$(shell uname -r)/build
	PWD += $(shell pwd)

default:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

clean:
	rm -f *.o *.ko .*.cmd *.order *.symvers

endif