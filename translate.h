/*
*  v1.0 09.06.2013 
* 
* 
*/
#ifndef __translate_
#define __translate_

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/stat.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <asm/uaccess.h>


#define TRANSLATE_DEVICE_NAME "translate"
#define TRANSLATE_DEVICE_COUNT 2
#define TRANSLATE_MAX_USERS 1
#define TRANSLATE_BUFFER_SIZE 40
#define TRANSLATE_CAESAR_SHIFT 3
#define TRANSLATE_MINOR 0
#define TRANSLATE_MAJOR 0
#define TRANSLATE_CAESAR_ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz"

MODULE_AUTHOR("Jonas Schaeufler");
MODULE_DESCRIPTION("A translation module for Caesar's code");
MODULE_VERSION("1.0");
MODULE_LICENSE("GPL");

static int major = TRANSLATE_MAJOR;
static int bufsize = TRANSLATE_BUFFER_SIZE;
static int cshift = TRANSLATE_CAESAR_SHIFT;

module_param(major, int, S_IRUGO);
module_param(bufsize, int, S_IRUGO);
module_param(cshift, int, S_IRUGO);

MODULE_PARM_DESC(bufsize, "buffer size of a device");
MODULE_PARM_DESC(cshift, "Offset for Caesar's shift");
MODULE_PARM_DESC(major, "Desired major number");


struct translate_dev {
  int fillcount, buf_size;
  int r_count, w_count;
  char *buf, *buf_end;
  char *pread, *pwrite;  
  wait_queue_head_t inq, outq;
  spinlock_t r_lock, w_lock;
  struct semaphore sem;
  struct cdev cdev;
};


int translate_init_module(void);
void translate_cleanup_module(void);


int translate_open(struct inode *inode, struct file *filp);
int translate_release(struct inode *inode, struct file *filp);
ssize_t translate_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
ssize_t translate_write(struct file *filp, const char __user *buf, size_t count,loff_t *f_pos);
ssize_t translate_write_dec(struct file *filp, const char __user *buf, size_t count,loff_t *f_pos);
ssize_t translate_write_enc(struct file *filp, const char __user *buf, size_t count,loff_t *f_pos);
void translate_setup_cdev(struct translate_dev *dev, int index);
void translate_caesar_encrypt(char *buf, int buf_size);
void translate_caesar_decrypt(char *buf, int buf_size);
void translate_caesar_shift(int right_shift, char *target_char, char *alphabet, int alphabet_size, int shift);

module_init(translate_init_module);
module_exit(translate_cleanup_module);

struct translate_dev *translate_devices;

struct file_operations translate_fops = {
  .owner = THIS_MODULE,
  .llseek = NULL,
  .read = translate_read,
  .write = translate_write,
  .open = translate_open,
  .release = translate_release,
  
};
struct file_operations translate_fops_dec = {
  .owner = THIS_MODULE,
  .llseek = NULL,
  .read = translate_read,
  .write = translate_write_dec,
  .open = translate_open,
  .release = translate_release,
  
};
struct file_operations translate_fops_enc = {
  .owner = THIS_MODULE,
  .llseek = NULL,
  .read = translate_read,
  .write = translate_write_enc,
  .open = translate_open,
  .release = translate_release,
  
};

#endif

#ifndef init_MUTEX
#define init_MUTEX(mutex) sema_init(mutex, 1)
#endif	