#!/bin/bash 

module="translate"
device="translate"
mode="664"


rm -f /dev/${device}[0-1]
/sbin/insmod ./$module.ko $* || exit 1

major=$(awk "\$2==\"$module\" {print \$1}" /proc/devices)
for i in  {0..1}
do
  mknod /dev/${device}$i c $major $i

  chgrp wheel /dev/${device}[0-1]
  chmod $mode /dev/${device}[0-1]
done

  modinfo $module.ko